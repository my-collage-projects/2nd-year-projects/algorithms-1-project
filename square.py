import turtle
turtle.speed(50)

def square(x,y,n):
    turtle.up()
    turtle.setpos(x + n/2,y +n/2)
    turtle.down()
    turtle.rt(90)
    turtle.fd(n)
    turtle.rt(90)
    turtle.fd(n)
    turtle.rt(90)
    turtle.fd(n)
    turtle.rt(90)
    turtle.fd(n)

def main(x,y,n,d):
    if  d > 0:
        square(x,y,n)
        main( x + n/2 , y + n/2 , n/2 , d-1)
        main( x + n/2 , y - n/2 , n/2 , d-1)
        main( x - n/2 , y - n/2 , n/2 , d-1)
        main( x - n/2 , y + n/2 , n/2 , d-1)

window = turtle.screensize()
main(0,0,300,5)
