import turtle

def Hilbert_curve(A,rule,t,n):

    if n>=1:
        if rule:
            t.left(90)
            Hilbert_curve(A,not rule,t, n-1)
            t.forward(A)
            t.right(90)
            Hilbert_curve(A, rule,t, n-1)
            t.forward(A)
            Hilbert_curve(A,rule,t, n-1)
            t.right(90)
            t.forward(A)
            Hilbert_curve(A,not rule,t, n-1)
            t.left(90)
        else:
            t.right(90)
            Hilbert_curve(A,rule,t, n-1)
            t.forward(A)
            t.left(90)
            Hilbert_curve(A,not rule,t, n-1)
            t.forward(A)
            Hilbert_curve(A,not rule,t, n-1)
            t.left(90)
            t.forward(A)
            Hilbert_curve(A, rule,t, n-1)
            t.right(90)

def main():
    A=10
    t=turtle.Turtle()
    my_win=turtle.Screen()
    n=2
    rule=True
    Hilbert_curve(A,rule,t,n)
    my_win.exitonclick()

main()
