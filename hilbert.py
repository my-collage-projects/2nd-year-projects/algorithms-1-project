import turtle

def D( i,h ):
    if i > 0 :
        A(i-1,h/2)
        turtle.rt(90)
        turtle.fd(h/2)
        turtle.lt(90)
        D(i-1,h/2)
        turtle.lt(180)
        turtle.fd(h/2)
        turtle.rt(180)
        D(i-1,h/2)
        turtle.lt(90)
        turtle.fd(h/2)
        turtle.rt(90)
        C(i-1,h/2)
    elif i == 0 :
        turtle.rt(90)
        turtle.fd(h)
        turtle.rt(90)
        turtle.fd(h)
        turtle.rt(90)
        turtle.fd(h)
        turtle.rt(90)
       
def A( i , h ) :
    if i > 0 :
        D(i-1,h/2)
        turtle.lt(90)
        turtle.fd(h/2)
        turtle.lt(180)
        A(i-1,h/2)
        turtle.rt(90)
        turtle.fd(h/2)
        turtle.lt(90)
        A(i-1,h/2)
        turtle.fd(h/2)
        B(i-1,h/2)
    elif i == 0 :
        turtle.rt(180)
        turtle.fd(h)
        turtle.lt(90)
        turtle.fd(h)
        turtle.lt(90)
        turtle.fd(h)


def C( i , h ) :
    if i > 0 :
        B(i-1,h/2)
        turtle.lt(90)
        turtle.fd(h/2)
        C(i-1,h/2)
        turtle.rt(90)
        turtle.fd(h/2)
        turtle.rt(90)
        C(i-1,h/2)
        turtle.fd(h/2)
        turtle.rt(180)
        D(i-1,h/2)
        turtle.lt(90)
    elif i == 0 :
        turtle.fd(h)
        turtle.lt(90)
        turtle.fd(h)
        turtle.lt(90)
        turtle.fd(h)

def B( i , h ) :
    if i > 0 :
        C(i-1,h/2)
        turtle.rt(90)
        turtle.fd(h/2)
        turtle.rt(90)
        B(i-1,h/2)
        turtle.lt(90)
        turtle.fd(h/2)
        B(i-1,h/2)
        turtle.fd(h/2)
        turtle.lt(90)
        A(i-1,h/2)
    elif i == 0 :
        turtle.lt(90)
        turtle.fd(h)
        turtle.rt(90)
        turtle.fd(h)
        turtle.rt(90)
        turtle.fd(h)
        
window = turtle.screensize()
D(2,100)

