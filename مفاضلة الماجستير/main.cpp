#include <iostream>
using namespace std;
#include<string.h>

struct student
{
    string name,res;
    int total;
    string a,b,c;
    student* next;
};

struct spl
{
    string s_name;
    int num;
};

int main()
{
    student* head = NULL ;
    spl sp_arr[8];
    int n,i=0,c=0;
    cout << " (SE) (IS) (M) (N) (IA) (KM) (CC) (SW) ";
    sp_arr[0].s_name= "SE";
    sp_arr[1].s_name= "IS";
    sp_arr[2].s_name= "M";
    sp_arr[3].s_name= "N";
    sp_arr[4].s_name= "IA";
    sp_arr[5].s_name= "KM";
    sp_arr[6].s_name= "CC";
    sp_arr[7].s_name= "SW";
    cout << "\nEnter numbers in :\n";
    for(i=0 ; i<8 ; i++)
    {
        cout << "\t\t"<< sp_arr[i].s_name << " : ";
        cin >> sp_arr[i].num;
    }
    cout << "\n\nEnter student number :  ";
    cin >> n;
    student* A[n];
    for ( i=0 ; i<n ; i++)
    {
        A[i]=0;
    }
    cout << " (SE) (IS)  (M)  (N) (IA) (KM) (CC) (SW) \n";
    for(int k=0 ; k<8 ; k++)
    {
        cout << "   " << sp_arr[k].num << " ";
    }
    cout << "\n";
    for( i=0; i < n ; i++)
       {

            cout << "\n---------------Student number ("<< i+1 << ")------------\n";
            student* s_new = new student;
            s_new->next=NULL;
            cout << "\nEnter student name :  ";
            cin >> s_new->name;
            cout << "\nEnter student average:  ";
            cin >> s_new->total;
            cout << "\nEnter student desires :";
            cout << "\n\t (1) :  ";
            cin >> s_new->a;
            cout << "\n\t (2) :  ";
            cin >> s_new->b;
            cout << "\n\t (3) :  ";
            cin >> s_new->c;
            A[i] = s_new;
            if(head == NULL)
            {
                head = s_new;
                head->next = NULL;
            }
            else if(head->total < s_new->total)
            {
                s_new->next = head;
                head = s_new;
            }
            else
            {
                student* temp = head;
                while (temp->next != NULL && temp->next->total > s_new->total)
                {
                    temp = temp->next;
                }
                s_new->next = temp->next;
                temp->next = s_new;
            }
    }
    student* temp = head;
    while(temp!=NULL)
    {
        c=0;
        for(int j=0 ; j<8 ; j++)
        {
            if(c==0)
            {
                if(sp_arr[j].s_name == temp->a)
                {
                    if(sp_arr[j].num > 0)
                    {
                        temp->res= sp_arr[j].s_name;
                        sp_arr[j].num--;
                        break;
                    }
                    else
                    {
                        j=0;
                        c=1;
                        continue;
                    }
                }
            }
            else if(c==1)
            {
                if(sp_arr[j].s_name == temp->b)
                {
                    if(sp_arr[j].num > 0)
                    {
                        temp->res= sp_arr[j].s_name;
                        sp_arr[j].num--;
                        break;
                    }
                    else
                    {
                        c=2;
                        j=0;
                        continue;
                    }
                }
            }
            else if(c==2)
            {
                if(sp_arr[j].s_name == temp->c)
                {
                    if(sp_arr[j].num > 0)
                    {
                        temp->res= sp_arr[j].s_name;
                        break;
                    }
                    else
                    {
                        c=3;
                        j=0;
                        continue;
                    }
                }
            }
            else if(c==3)
            {
                temp->res = " ";
                break;
            }
        }
        temp = temp->next;
    }

    for( i=0 ; i<n ; i++)
    {
        cout << "\n\n"<< A[i]->name  << "    ";
        if( A[i]->res  == " ")
        {
            cout << "none\n\n";
        }
        else
        {
            cout << A[i]->res << "\t";
        }
    }

    return 0;
}

