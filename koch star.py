import turtle
turtle.speed(50)

def koch(i,n) :
    if i == 0 :
        turtle.fd(n/3)
    else :
        koch(i-1,n/3)
        turtle.lt(60)
        koch(i-1,n/3)
        turtle.rt(120)
        koch(i-1,n/3)
        turtle.lt(60)
        koch(i-1,n/3)

def star(i,n) :
    if  i > 0 :
        turtle.rt(60)
        koch(i,n/3)
        turtle.rt(120)
        koch(i,n/3)
        turtle.rt(120)
        koch(i,n/3)
        turtle.rt(60)

        window = turtle.screensize()
turtle.up()
turtle.setpos(0,100)
turtle.down()
n = int (input())
l=700*n
star(n,5)
turtle.exitionclick()
