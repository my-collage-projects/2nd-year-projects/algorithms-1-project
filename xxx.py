import turtle

turtle.speed(50)

def draw(x,y,n):
    turtle.up()
    turtle.setpos(x,y)
    turtle.down()
    turtle.lt(45)
    turtle.fd(n)
    turtle.fd(-2*n)
    turtle.fd(n)
    turtle.rt(90)
    turtle.fd(n)
    turtle.fd(-2*n)
    turtle.lt(45)

def main(x,y,n,d):
    if d > 0 :
        draw(x,y,n)
        main( x+n , y+n , n/2 , d-1 )
        main( x-n , y+n , n/2 , d-1 )
        main( x-n , y-n , n/2 , d-1 )
        main( x+n , y-n , n/2 , d-1 )

window = turtle.screensize()
main(0,0,100,5)
