import turtle
turtle.speed(50)
def draw(l,d) :
    if  d > 0 :
        turtle.fd(l)
        turtle.rt(90)
        draw(2*l/3,d-1)
        turtle.rt(90)
        turtle.fd(l*2)
        turtle.rt(90)
        draw(2*l/3,d-1)
        turtle.rt(90)
        turtle.fd(l)

window = turtle.screensize()
draw(200,8)
turtle.exitionclick()
