import turtle
turtle.speed(100)
def drawH(x,y,L):
    turtle.pencolor("blue")
    turtle.up()
    turtle.setpos(x,y)
    turtle.down()
    turtle.fd(L)
    turtle.rt(90)
    turtle.fd(-L)
    turtle.fd(2*L)
    turtle.fd(-L)
    turtle.rt(90)
    turtle.fd(2*L)
    turtle.rt(90)
    turtle.fd(-L)
    turtle.fd(2*L)
    turtle.fd(-L)
    turtle.rt(90)

def draw(x,y,L,i) :
    if i==0 :
        return

    drawH(x,y,L)
    draw( x + L , y + L , L / 2 , i-1)
    draw( x + L , y - L , L / 2 , i-1)
    draw( x - L , y - L , L / 2 , i-1)
    draw( x - L , y + L , L / 2 , i-1)

draw(0,0,100,4)
turtle.exitionclick()

