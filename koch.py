import turtle
turtle.speed(50)

def koch(i,n) :
    if i == 0 :
        turtle.fd(n/3)
    else :
        koch(i-1,n/3)
        turtle.lt(60)
        koch(i-1,n/3)
        turtle.rt(120)
        koch(i-1,n/3)
        turtle.lt(60)
        koch(i-1,n/3)
        
window = turtle.screensize()
turtle.up()
turtle.setpos(-200,0)
turtle.down()
koch(6,1900)
turtle.exitionclick()
